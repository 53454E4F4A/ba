'''
Created on 08.09.2015

@author: jones
'''


import requests
def buildHTTP_resp(response, ip):
    statusline = "HTTP/1.1 200 OK\r\n";
    headers = ""
    for key,value in response.headers.iteritems():
            headers += key + ": " + value+"\r\n";
     
    headers += "x-client-address: "+ ip+"\r\n";
    
    return statusline + headers

r = requests.get("http://google.de", verify=False)

print(buildHTTP_resp(r, "10.10.11.22"))

