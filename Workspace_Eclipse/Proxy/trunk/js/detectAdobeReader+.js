/*

    detectAdobeReader+.js v0.3.2
    By Eric Gerds   http://www.pinlady.net/PluginDetect/

    Detect Adobe PDF Reader plugin & PDF.js (Firefox native PDF viewer), 
    and also determine when Firefox is using PDF.js by default instead of the Adobe plugin.


 Note:
  1) Download the PluginDetect script. You need to select BOTH the "Adobe Reader" option
     AND the "PDF.js" option on the PluginDetect download page, otherwise this script
     will not work properly.
  2) Also download the DummyPDF_pdfjs file (empty.pdf), and the DummyPDF_adobe file (detectAdobe.pdf).
     Adjust the paths of the "DummyPDF_pdfjs" and "DummyPDF_adobe" variables
     in this script.
  2) In your web page, load the PluginDetect script BEFORE you load this script.
  3) In your web page, have the output <div> BEFORE this script. The <div> looks like this:
       <div id="detectAdobeRdr+_output"></div>
  4) Feel free to modify this script as you wish.
  5) If you wish to specify the plugindetect <div>, then insert it into the <body>,
     anywhere BEFORE the plugin detection begins.
     This <div> temporarily holds any plugin object that is inserted into the web page,
     but only when needed for detection.
     For example:
        <div id="plugindetect" style="right:0px; top:0px; position:absolute;"></div>
     This step is optional, as PluginDetect will create and insert the <div> automatically 
     when needed.


*/


(function(){
  

 var $=PluginDetect,

   AdobePlugin = {name:"AdobeReader", status:-3, version:null, minVersion:"11,0,0,0", 
      info:null, done:0, id:1},

   PDFjsPlugin = {name:"PDFjs", status:-3, version:null, minVersion:"0", 
      info:null, done:0, id:2},

   // The DummyPDF path can be relative or absolute.
   // If using a relative path, then remember that the path is relative to the web page itself.
   //
   // We only need to include DummyPDF in the onDetectionDone() method, since it will
   // automatically pass along the DummyPDF to any PluginDetect command used within
   // the onDetectionDone event handler.
   DummyPDF_pdfjs = "../files/empty.pdf",    // for PDFjs
   DummyPDF_adobe = "../files/detectAdobe.pdf";  // for Adobe Reader

   // node for output text
   out = document.getElementById("detectAdobeRdr+_output");



 // Return text message based on plugin detection result
 var getStatusMsg = function(obj)
 {
   var Msg1 = " [PDF documents may be displayed using your browser with the Adobe plugin ";
   var Msg2 = "(but <object>/<embed> tags cannot be used) ";
   var Msg3 = "and/or using the Adobe Reader standalone application.]";

   if (obj.status==1) return "installed & enabled, version is >= " +
          obj.minVersion + Msg1 + Msg3;
   if (obj.status == -0.5 || obj.status == 0.5)
          return "detection not completed yet, requires NOTF detection";
   if (obj.status==0) return "installed & enabled, version is unknown" + Msg1 + Msg3;
   if (obj.status==-0.1) return "installed & enabled, version is < " +
          obj.minVersion + Msg1 + Msg3;
   if (obj.status==-0.15) return "installed but not enabled for <object>/<embed> tags. " +
      "This result occurs for Internet Explorer when the Adobe Reader ActiveX " +
      "control is disabled in the add-ons menu." + Msg1 + Msg2 + Msg3;
   if (obj.status==-1) return "not installed or not enabled " +
      "[The browser plugin is not installed/not enabled. However, it is still possible " +
      "that the Adobe Reader standalone application may be on your computer and can " +
      "display PDF documents. Note: PluginDetect can only detect browser plugins, " +
      "not standalone applications.]";

   if (obj.status==-1.2) return "installed, " + 
      "but a non-Adobe PDF viewer is currently the default PDF viewer in your browser. " +
      "Your browser will most likely use this non-Adobe viewer to display PDF documents.";
   if (obj.status==-1.3) return "not installed/not enabled, " +
      "and a non-Adobe PDF viewer is currently the default PDF viewer in your browser. " +
      "Your browser will most likely use this non-Adobe viewer to display PDF documents.";

   if (obj.status==-1.5) return "unknown " +
      "[Unable to determine if the Adobe Reader plugin is installed and able " +
      "to display PDF documents in your browser. " +
      "This result occurs for Internet Explorer when ActiveX is disabled and/or " +
      "ActiveX Filtering is enabled. " +
      "Note: the Adobe Reader plugin can display a PDF document with or without " +
      "ActiveX in Internet Explorer. Without ActiveX, however, we cannot detect " +
      "the presence of the plugin and we cannot use <object>/<embed> tags to display a PDF.]";

   if (obj.status==-3) return "error...bad input argument to PluginDetect method";
   return "unknown";

 };   // end of getStatusMsg()


 // Add text to output node
 var docWrite = function(text)
 {
     if (out){
        if (text){
          text = text.replace(/&nbsp;/g,"\u00a0");
          out.appendChild(document.createTextNode(text));
        };
        out.appendChild(document.createElement("br"));
     };
 };



 function ShowResult($, plugin1, plugin2)
 {
    if ($.isMinVersion) plugin1.status = $.isMinVersion(plugin1.name, plugin1.minVersion);
    if ($.getVersion) plugin1.version = $.getVersion(plugin1.name);
    if ($.getInfo) plugin1.info = $.getInfo(plugin1.name);
    plugin1.done=1;

    if (!plugin2.done) return;
    
    // When we reach this point, all plugin detections have completed.
    // Now we show the results.

    var adobe = plugin1.id==1 ? plugin1 : plugin2,
      pdfjs = plugin1.id==2 ? plugin1 : plugin2;


    // If PDFjs is the default PDF viewer,
    // then Adobe Reader will not be used by the browser.
    if (pdfjs.status >=0)
      // If Adobe Reader plugin is installed, then -1.2.
      // If Adobe Reader plugin is not installed/not enabled, then -1.3.
      adobe.status = adobe.status >= -0.2 ? -1.2: -1.3;


    docWrite("Adobe plugin version: " + adobe.version);
    docWrite("");

    docWrite("Adobe plugin status: " + getStatusMsg(adobe));
    docWrite("");

    // Get extra plugin info
    var INFO = adobe.info;
    if (INFO)
    {
         var precision = INFO.precision; // 0 - 4
         precision = (["A", "B", "C", "D"].slice(0, precision)).concat(["0", "0", "0", "0"].slice(0,4-precision)).join();
         docWrite("Detected version precision: " + precision);

         docWrite("");
    }


    if ($.browser.isIE)
    {
       docWrite("ActiveX enabled / ActiveX scripting enabled: " +
         ($.browser.ActiveXEnabled ? "true" : "false [this prevents detection of the plugin in Internet Explorer]")
       );
       docWrite("ActiveX Filtering enabled: " +
         ($.browser.ActiveXFilteringEnabled ? "true [this prevents detection of the plugin in Internet Explorer]" : "false")
       );
    };


 }; // end of ShowResult()


 // if onDectionDone() > 0, then detection done
 // if == 0, detection not done yet
 // if < 0, then error. The pluginName specified is not in the PluginDetect script.
 //    [You may have forgotten to select that plugin on the download page when creating the script].
 if ($.onDetectionDone)
 {
    $.onDetectionDone(AdobePlugin.name, [ShowResult, AdobePlugin, PDFjsPlugin], DummyPDF_adobe);

    $.onDetectionDone(PDFjsPlugin.name, [ShowResult, PDFjsPlugin, AdobePlugin], DummyPDF_pdfjs);
 };



})();    // end of function



