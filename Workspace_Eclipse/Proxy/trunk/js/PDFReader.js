/*
     PDF Reader detector   v0.3.2
     By Eric Gerds   http://www.pinlady.net/PluginDetect/

     Detect if a browser is able to display PDF documents, either natively or using a plugin.


 USAGE:
  1) This script works together with the PluginDetect script.
     See http://www.pinlady.net/PluginDetect/download/

  2) Insert the PluginDetect script into the <head> or <body> of your web page.

  3) The output <div> is assumed to be in the parent window of your web page.
     Have the output <div> BEFORE this script. The <div> looks like this:
       <div id="pdfresult"></div>

  4) If you wish to specify the plugindetect <div>, then insert it into the <body>,
     anywhere BEFORE the plugin detection begins.
     This <div> temporarily holds any plugin object that is inserted into the web page,
     but only when needed for detection.
     For example:
        <div id="plugindetect" style="right:0px; top:0px; position:absolute;"></div>
     This step is optional, as PluginDetect will create and insert the <div> automatically 
     when needed.

  5) Insert this script AFTER the PluginDetect script, AFTER the output <div>,
     and AFTER the plugindetect <div> (assuming you specifed the plugindetect <div>).
     The PluginDetect script and this script should both be within the same DOM window object.
     You can place them both within the top most DOM window of a web page, or both within a
     frame (or iframe).

  6) Get a copy of the "empty.pdf" file. The empty.pdf file is needed for PDF detection.
     Adjust the value of the "DummyPDF" variable (given in the code below) to reflect the
     empty.pdf name and path.

     Examples...

     If your web page is at      http://www.mysite.com/webpage.htm
     and you have empty.pdf at   http://www.mysite.com/empty.pdf
     then DummyPDF = "empty.pdf"  (relative path, relative to the web page)

     If your web page is at       http://www.mysite.com/webpage.htm
     and you have empty.pdf at    http://www.mysite.com/stuff/empty.pdf
     then DummyPDF = "stuff/empty.pdf"   (relative path, relative to the web page)
     or   DummyPDF = "/stuff/empty.pdf"  (absolute path)

  7) Feel free to change this script, remove comments, etc... to fit your own needs.


*/


(function(){

var $ = PluginDetect, 

  // The DummyPDF path can be relative or absolute.
  // Only the very first PDFReader PluginDetect command that is executed
  // needs to have the DummyPDF input argument. You do not have to specify this input arg in
  // any subsequent PDFReader PluginDetect commands.
  DummyPDF = "../files/empty.pdf",
  
  // If ==1, then we try to detect non-Adobe PDF Readers (in addition to Adobe Reader)
  //     for Internet Explorer. It is very possible that this will cause a security popup
  //     to occur in IE when certain non-Adobe PDF Readers are encountered.
  // If ==0, then we only try to detect Adobe Reader for IE. This should not cause
  //     any security popups in IE.
  detectNonAdobeIE = 1;

// node for output text
var out = document.getElementById("pdfresult");



// Return text message based on plugin status
var getStatusMsg = function(status)
{
   var Msg1 = " [PDF documents may be displayed using your browser with a PDF Reader plugin ";
   var Msg2 = "(but <object>/<embed> tags cannot be used) ";
   var Msg3 = "and/or using a PDF Reader standalone application.]";

   if (status==0) return "installed & enabled" + Msg1 + Msg3;
   if (status==-0.15) return "installed but not enabled for <object>/<embed> tags" + 
      Msg1 + Msg2 + Msg3;
   if (status==-0.5) return "detection has started but has not completed yet";
   if (status==-1) return "not installed or not enabled " +
      "[No PDF Reader plugin appears to be present in your browser. However, it is still possible " +
      "that a PDF Reader standalone application may be on your computer and can " +
      "display PDF documents. Note: PluginDetect can only detect browser plugins, " +
      "not standalone applications.]";
   if (status==-1.5) return "unknown " +
      "[Unable to determine if a PDF Reader plugin is installed and able " +
      "to display PDF documents in your browser. " +
      "This result occurs for Internet Explorer when " + 
      "A) ActiveX is disabled, or " +
      "B) ActiveX Filtering is enabled, or " +
      "C) \"detectNonAdobeIE\" is false/undefined and Adobe Reader was not detected. " +
      "Note: a PDF Reader plugin can display a PDF document with or without " +
      "ActiveX in Internet Explorer. Without ActiveX, however, we cannot detect " +
      "the presence of the plugin and we cannot use <object>/<embed> tags to display a PDF.]";

   if (status==-3) return "error...bad input argument to PluginDetect method";
   return "unknown";

};   // end of getStatusMsg()


// Add text to output node
var docWrite = function(text){
     if (out)
     {
        if (text)
        {
          text = text.replace(/&nbsp;/g,"\u00a0");
          out.appendChild(document.createTextNode(text));
        };
        out.appendChild(document.createElement("br"));
     };
 };  // end of function


function displayPDFresults($)
{

   var status, msg;

   if ($.isMinVersion)
   {
      status = $.isMinVersion("PDFReader", 0);
      docWrite("PDF Reader plugin status: " + getStatusMsg(status));
      docWrite("");
   };

   docWrite("Browser has \"application/pdf\" in navigator.mimeTypes array: " +
      ($.hasMimeType("application/pdf") ? "true" : "false"));


   // Get extra info on the plugin.
   var INFO = $.getInfo ? $.getInfo("PDFReader") : null;
   if (INFO)
   {
      msg = "PDF detection: ";
      if (INFO.OTF==0) msg += "completed ON THE FLY (OTF)";
      else if (INFO.OTF==2) msg += "completed NOT ON THE FLY (NOTF)";
      else if (INFO.OTF==1) msg += "not completed yet, requires NOTF detection";
      docWrite(msg);

      docWrite("DummyPDF file was used for detection in this browser: " +
          (INFO.DummyPDFused ? "true" : "false"));
   };


   if ($.browser.isIE)
   {
      docWrite("");
      docWrite("ActiveX enabled / ActiveX scripting enabled: " +
        ($.browser.ActiveXEnabled ? "true" : "false [this will prevent the <object/embed> tag from displaying a PDF in Internet Explorer]")
      );
      docWrite("ActiveX Filtering enabled: " +
        ($.browser.ActiveXFilteringEnabled ? "true [this will prevent the <object/embed> tag from displaying a PDF in Internet Explorer]" : "false")
      );
   };


};   // end of displayPDFresults()


// Display results when PDF detection is completed
$.onDetectionDone("PDFReader", displayPDFresults, DummyPDF, detectNonAdobeIE);



})();   // end of function

