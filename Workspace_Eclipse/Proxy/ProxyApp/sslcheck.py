# -*- coding: latin-1 -*-
#
# Copyright (C) AB Strakt
# Copyright (C) Jean-Paul Calderone
# See LICENSE for details.

"""
Simple SSL client, using blocking I/O
"""
import csv
import OpenSSL
from OpenSSL import SSL, crypto
import sys, os, select, socket


def verify_cb(conn, cert, errnum, depth, ok):
 
    commonname = cert.get_subject().commonName
    print('Got certificate: ' + commonname )
    print( cert.get_subject())
    return ok





def ssl_check_info(conn, where, ret):
    print(conn)

def ssl_check(addr, port):
    with open (os.path.join(os.path.dirname(__file__), 'ciphers.csv'), "r") as cipherfile:
        cipherreader = csv.reader(cipherfile);
        cipherlist = list(cipherreader)[0];
    
    methodstring = [
    "test",
    "SSLv2",
    "SSLv3",
    "SSLv23",
    "TLSv1",
    "TLSv1_1",
    "TLSv1_2"]

    methodData = []
    cipherPerMethod = {}		

    for method in range(1, 7):
        

        allowed_ciphers = []
        try:
            ctx = SSL.Context(method)
            ctx.use_privatekey_file (os.path.join(os.path.dirname(__file__),  'client.pkey'))
            ctx.use_certificate_file(os.path.join(os.path.dirname(__file__), 'client.cert'))
            ctx.load_verify_locations(os.path.join(os.path.dirname(__file__),  'CA.cert'))


            count = 0;
            results = []
            for cipher in cipherlist:

                try:
                    ctx.set_cipher_list(cipher)
                
                    sock = SSL.Connection(ctx, socket.socket(socket.AF_INET, socket.SOCK_STREAM))
                    sock.connect((addr, port))
                    sock.get_cipher_list();
                    sock.set_connect_state()
                    
                    try:
                        sock.do_handshake()
    
                        if sock.get_cipher_name() is not None:
                            print(sock.get_cipher_name())
                            allowed_ciphers.append(str(sock.get_cipher_name()))
                            results.append(sock.get_cipher_name())
                    except SSL.Error as errors:
                        print("Handshake exception: ") 
                        results.append(errors)
                    sock.close()
                except SSL.Error as errors:
                    results.append(errors);
                    print("Connect exception: ") 
            #print(results);
            methodData.append(results);
            cipherPerMethod[methodstring[method] + "_ciphers"] = allowed_ciphers;
            #print(results)
        except ValueError as errors:
            print(methodstring[method]);
            methodData.append(errors)
            cipherPerMethod[methodstring[method] + "_ciphers"] = "None";
           
            

    return cipherPerMethod