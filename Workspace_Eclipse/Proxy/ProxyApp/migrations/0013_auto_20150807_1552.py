# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0012_auto_20150807_1551'),
    ]

    operations = [
        migrations.CreateModel(
            name='Clientz',
            fields=[
                ('ip', models.CharField(max_length=15, serialize=False, primary_key=True)),
                ('user_agent', models.CharField(max_length=255)),
                ('appName', models.TextField(default=b'')),
                ('appCodeName', models.TextField(default=b'')),
                ('appVersion', models.TextField(default=b'')),
                ('plugin_count', models.IntegerField(default=0)),
                ('plugins', models.TextField(default=b'')),
                ('mimeType_count', models.IntegerField(default=0)),
                ('mimeTypes', models.TextField(default=b'')),
                ('javaEnabled', models.TextField(default=b'')),
                ('language', models.TextField(default=b'')),
                ('cookieEnabled', models.TextField(default=b'')),
                ('platform', models.TextField(default=b'')),
                ('product', models.TextField(default=b'')),
                ('ciphers', models.TextField(default=b'')),
            ],
        ),
        migrations.DeleteModel(
            name='Clients',
        ),
    ]
