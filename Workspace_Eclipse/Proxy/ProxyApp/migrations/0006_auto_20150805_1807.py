# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0005_server'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='cookieEnabled',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='client',
            name='javaEnabled',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='client',
            name='language',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='client',
            name='platform',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='client',
            name='product',
            field=models.TextField(default=''),
        ),
    ]
