# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0023_remove_client_forbidden_media_types'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='filterTypes',
            field=models.TextField(default=b''),
        ),
    ]
