# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0002_auto_20150731_1652'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='plugins',
            field=models.TextField(default=''),
        ),
    ]
