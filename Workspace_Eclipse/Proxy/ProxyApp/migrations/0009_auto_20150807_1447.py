# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0008_auto_20150807_0429'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='appCodeName',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='client',
            name='appName',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='client',
            name='appVersion',
            field=models.TextField(default=b''),
        ),
    ]
