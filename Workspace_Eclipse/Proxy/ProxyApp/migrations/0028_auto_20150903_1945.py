# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0027_client_sslfootprint'),
    ]

    operations = [
        migrations.RenameField(
            model_name='client',
            old_name='ssl23ciphers',
            new_name='ssl23enabled',
        ),
        migrations.RenameField(
            model_name='client',
            old_name='ssl2ciphers',
            new_name='ssl2enabled',
        ),
        migrations.RenameField(
            model_name='client',
            old_name='ssl3ciphers',
            new_name='ssl3enabled',
        ),
        migrations.RenameField(
            model_name='client',
            old_name='tls11ciphers',
            new_name='tls11enabled',
        ),
        migrations.RenameField(
            model_name='client',
            old_name='tls12ciphers',
            new_name='tls12enabled',
        ),
        migrations.RenameField(
            model_name='client',
            old_name='tls1ciphers',
            new_name='tls1enabled',
        ),
    ]
