# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0021_auto_20150822_1737'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='forbidden_media_types',
            field=models.TextField(default=b''),
        ),
    ]
