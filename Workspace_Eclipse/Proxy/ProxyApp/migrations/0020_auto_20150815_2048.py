# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0019_auto_20150815_2015'),
    ]

    operations = [
        migrations.AddField(
            model_name='mimetype',
            name='allowed',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='plugin',
            name='allowed',
            field=models.BooleanField(default=True),
        ),
    ]
