# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0004_auto_20150801_1826'),
    ]

    operations = [
        migrations.CreateModel(
            name='Server',
            fields=[
                ('hostname', models.CharField(primary_key=True, max_length=255, serialize=False)),
                ('ip', models.CharField(max_length=16)),
                ('google_safe_browsing_status_code', models.IntegerField(default=0)),
                ('google_safe_browsing_body', models.CharField(max_length=128)),
            ],
        ),
    ]
