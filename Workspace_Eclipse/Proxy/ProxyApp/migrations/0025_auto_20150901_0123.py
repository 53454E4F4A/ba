# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0024_client_filtertypes'),
    ]

    operations = [
        migrations.CreateModel(
            name='HTTPHeader',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField(default=b'')),
                ('allowed', models.BooleanField(default=True)),
            ],
        ),
        migrations.AddField(
            model_name='server',
            name='bl_alien',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='bl_bfb',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='bl_chaos',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='bl_cleanmx',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='bl_dragon',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='bl_feodo',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='bl_malcode',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='bl_mdl',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='bl_openbl',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='bl_ptank',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='bl_spamhs',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='bl_zeus',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='city',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='countrycode',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='countryname',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='regioncode',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='regionname',
            field=models.TextField(default=b''),
        ),
    ]
