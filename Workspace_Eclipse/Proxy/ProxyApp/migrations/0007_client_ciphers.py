# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0006_auto_20150805_1807'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='ciphers',
            field=models.TextField(default=b''),
        ),
    ]
