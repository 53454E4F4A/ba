# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0025_auto_20150901_0123'),
    ]

    operations = [
        migrations.AddField(
            model_name='server',
            name='blacklisted',
            field=models.IntegerField(default=0),
        ),
    ]
