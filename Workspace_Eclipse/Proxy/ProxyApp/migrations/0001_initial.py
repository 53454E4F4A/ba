# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('client_ip', models.CharField(serialize=False, primary_key=True, max_length=15)),
                ('user_agent', models.CharField(max_length=255)),
            ],
        ),
    ]
