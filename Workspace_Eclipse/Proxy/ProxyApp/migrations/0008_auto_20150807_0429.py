# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0007_client_ciphers'),
    ]

    operations = [
        migrations.AddField(
            model_name='server',
            name='SSLv23_ciphers',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='SSLv2_ciphers',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='SSLv3_ciphers',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='TLSv1_1_ciphers',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='TLSv1_2_ciphers',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='TLSv1_ciphers',
            field=models.TextField(default=b''),
        ),
    ]
