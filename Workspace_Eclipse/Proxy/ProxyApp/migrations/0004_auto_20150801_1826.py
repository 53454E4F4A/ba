# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0003_client_plugins'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='mimeType_count',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='client',
            name='mimeTypes',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='client',
            name='plugin_count',
            field=models.IntegerField(default=0),
        ),
    ]
