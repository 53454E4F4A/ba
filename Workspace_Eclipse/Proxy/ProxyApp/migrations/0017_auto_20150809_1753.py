# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0016_auto_20150807_2209'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='blocked',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='client',
            name='blockedmessage',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='client',
            name='info',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='client',
            name='infomessage',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='blocked',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='server',
            name='blockedmessage',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='server',
            name='info',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='server',
            name='infomessage',
            field=models.TextField(default=b''),
        ),
    ]
