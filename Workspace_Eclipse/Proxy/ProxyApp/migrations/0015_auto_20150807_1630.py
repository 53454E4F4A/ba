# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0014_auto_20150807_1556'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='cookieEnabled',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='client',
            name='javaEnabled',
            field=models.BooleanField(default=False),
        ),
    ]
