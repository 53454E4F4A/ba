# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0018_auto_20150815_1842'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, default=1, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='client',
            name='ip',
            field=models.CharField(max_length=15),
        ),
        migrations.AlterUniqueTogether(
            name='client',
            unique_together=set([('ip', 'user_agent')]),
        ),
    ]
