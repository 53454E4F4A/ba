# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0011_auto_20150807_1551'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Client',
            new_name='Clients',
        ),
    ]
