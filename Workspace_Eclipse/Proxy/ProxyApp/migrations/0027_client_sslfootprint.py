# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0026_server_blacklisted'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='sslfootprint',
            field=models.TextField(default=b''),
        ),
    ]
