# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0015_auto_20150807_1630'),
    ]

    operations = [
        migrations.RenameField(
            model_name='server',
            old_name='google_safe_browsing_body',
            new_name='sb_desc',
        ),
        migrations.RenameField(
            model_name='server',
            old_name='google_safe_browsing_status_code',
            new_name='sb_status',
        ),
    ]
