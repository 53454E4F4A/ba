# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0017_auto_20150809_1753'),
    ]

    operations = [
        migrations.CreateModel(
            name='mimeType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mimeType', models.TextField(default=b'')),
                ('fileExt', models.TextField(default=b'')),
            ],
        ),
        migrations.CreateModel(
            name='Plugin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField(default=b'')),
                ('fileName', models.TextField(default=b'')),
                ('description', models.TextField(default=b'')),
            ],
        ),
        migrations.RemoveField(
            model_name='client',
            name='mimeType_count',
        ),
        migrations.RemoveField(
            model_name='client',
            name='plugin_count',
        ),
        migrations.RemoveField(
            model_name='client',
            name='mimeTypes',
        ),
        migrations.RemoveField(
            model_name='client',
            name='plugins',
        ),
        migrations.AddField(
            model_name='mimetype',
            name='plugin',
            field=models.ForeignKey(to='ProxyApp.Plugin'),
        ),
        migrations.AddField(
            model_name='client',
            name='mimeTypes',
            field=models.ManyToManyField(to='ProxyApp.mimeType'),
        ),
        migrations.AddField(
            model_name='client',
            name='plugins',
            field=models.ManyToManyField(to='ProxyApp.Plugin'),
        ),
    ]
