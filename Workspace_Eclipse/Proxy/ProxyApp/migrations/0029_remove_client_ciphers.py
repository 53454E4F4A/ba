# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0028_auto_20150903_1945'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='ciphers',
        ),
    ]
