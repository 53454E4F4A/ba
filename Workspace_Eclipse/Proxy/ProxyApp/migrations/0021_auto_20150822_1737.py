# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProxyApp', '0020_auto_20150815_2048'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='ssl23ciphers',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='client',
            name='ssl2ciphers',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='client',
            name='ssl3ciphers',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='client',
            name='tls11ciphers',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='client',
            name='tls12ciphers',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='client',
            name='tls1ciphers',
            field=models.TextField(default=b''),
        ),
    ]
