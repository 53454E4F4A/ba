import requests
import urllib
import json


def metascan_request(url):
    metascan_url = "https://ipscan.metascan-online.com/v1/scan/"
    headers = {'apikey' : 'd01fb28a2928871c84462d49368bae60'}
    r = requests.get(metascan_url + urllib.quote(url), verify=False, headers=headers) 
    print(r.status_code)
    data = json.loads(r.content)
    print(data)
    return data


url = "http://www.benchblog.com"
data = metascan_request(url)
print(data)
     
