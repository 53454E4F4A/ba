from django.contrib import admin
from proxy.models import Host
# Register your models here.

class HostAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['hostname']}),
        ('send_cookies', {'fields': ['send_cookies']}),    
        ('set_cookies_allowed', {'fields': ['set_cookies_allowed']}),    
        ('allowed', {'fields': ['allowed']}),    
        ('allow_javascript', {'fields': ['allow_javascript']}),  
  
    ]


admin.site.register(Host, HostAdmin)