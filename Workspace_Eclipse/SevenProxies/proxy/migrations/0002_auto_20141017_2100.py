# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proxy', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='blacklist',
            name='reason',
        ),
        migrations.RemoveField(
            model_name='blacklist',
            name='since',
        ),
        migrations.RemoveField(
            model_name='blacklist',
            name='until',
        ),
        migrations.RemoveField(
            model_name='whitelist',
            name='reason',
        ),
        migrations.RemoveField(
            model_name='whitelist',
            name='since',
        ),
        migrations.RemoveField(
            model_name='whitelist',
            name='until',
        ),
    ]
