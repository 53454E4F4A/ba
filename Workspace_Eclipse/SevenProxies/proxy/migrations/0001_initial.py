# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Blacklist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('domain', models.CharField(max_length=200)),
                ('reason', models.CharField(max_length=200)),
                ('since', models.DateTimeField(verbose_name='date blacklisted')),
                ('until', models.DateTimeField(verbose_name='date expire')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Whitelist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('domain', models.CharField(max_length=200)),
                ('reason', models.CharField(max_length=200)),
                ('since', models.DateTimeField(verbose_name='date whitelisted')),
                ('until', models.DateTimeField(verbose_name='date expire')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
