sslcheck_addr = 'https://192.168.178.20';

function cors_request(method, url){
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr){       
         xhr.open(method, url, false);
		
    } else if (typeof XDomainRequest != "undefined"){
        xhr = new XDomainRequest();
        xhr.open(method, url);
        xhr.onload = function() {
         alert(xhr.responseText);
            }
    } else {
        xhr = null;
    }

    return xhr;
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function post(path, params, method) {
    method = method || "post";
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
			form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}
var data = {};


var plugins = "";
data["plugin_count"] = navigator.plugins.length;
for (var i = 0; i < navigator.plugins.length; i++) {
  plugins = plugins + navigator.plugins[i].name + "\t" +  navigator.plugins[i].filename + "\t" + navigator.plugins[i].description + "\n";

}
mimeTypes = "";
data["mimeType_count"] = navigator.mimeTypes.length;
for (var i = 0; i < navigator.mimeTypes.length; i++) {
    mimeTypes = mimeTypes + navigator.mimeTypes[i].type + "\t" +  navigator.mimeTypes[i].suffixes + "\t" + navigator.mimeTypes[i].description + "\t" + 
    navigator.mimeTypes[i].enabledPlugin.name + "\t" + navigator.mimeTypes[i].enabledPlugin.filename +
    "\t" + navigator.mimeTypes[i].enabledPlugin.description + "\n";
}


try {
   xhr = cors_request('GET', 'https://' +sslcheck_addr +':8081/');
xhr.send();
data["SSLv2_Ciphers"] = xhr.responseText;
}catch(err) {
    data["SSLv2_Ciphers"] = "Disabled";
}
try {
   xhr = cors_request('GET', 'https://' +sslcheck_addr +':8082/');
xhr.send();
data["SSLv3_Ciphers"] = xhr.responseText;

}catch(err) {
    data["SSLv3_Ciphers"] = "Disabled";
}
try {
   xhr = cors_request('GET', 'https://' +sslcheck_addr +':8083/');
xhr.send();
data["SSLv23_Ciphers"] = xhr.responseText;

}catch(err) {
    data["SSLv23_Ciphers"] = "Disabled";
}
try {
   xhr = cors_request('GET', 'https://' +sslcheck_addr +':8084/');
xhr.send();
data["TLSv1_Ciphers"] = xhr.responseText;

}catch(err) {
    data["TLSv1_Ciphers"] = "Disabled";
}
try {
   xhr = cors_request('GET', 'https://' +sslcheck_addr +':8085/');
xhr.send();
data["TLSv1_1_Ciphers"] = xhr.responseText;

}catch(err) {
    data["TLSv1_1_Ciphers"] = "Disabled";
}
try {
   xhr = cors_request('GET', 'https://' +sslcheck_addr +':8086/');
xhr.send();
data["TLSv1_2_Ciphers"] = xhr.responseText;

}catch(err) {
    data["TLSv1_2_Ciphers"] = "Disabled";
}


data["mimeTypes"] = mimeTypes;
data["plugins"] = plugins;
data["javaEnabled"] = navigator.javaEnabled();
data["appCodeName"] = navigator.appCodeName;
data["appName"] = navigator.appName;
data["appVersion"] = navigator.appVersion;
data["cookieEnabled"] = navigator.cookieEnabled;
data["language"] = navigator.language;
data["onLine"] = navigator.onLine;
data["platform"] = navigator.platform;
data["product"] = navigator.product;
data["userAgent"] = navigator.userAgent;
data["referer"] = getParameterByName("refer");
post('http://www.google.com/newuser/', data);