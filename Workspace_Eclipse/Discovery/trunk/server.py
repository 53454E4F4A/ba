# -*- coding: latin-1 -*-
#
# Copyright (C) AB Strakt
# Copyright (C) Jean-Paul Calderone
# See LICENSE for details.
# Modified by: Jonas Sch�ufler
"""
Simple echo server, using nonblocking I/O
Modification: gets from client supported ciphers and returns JavaScript code to extract browser configuration data. 
"""
import OpenSSL
from OpenSSL import SSL, crypto
import sys, os, select, socket


def verify_cb(conn, cert, errnum, depth, ok):
    certsubject = crypto.X509Name(cert.get_subject())
    commonname = certsubject.commonName
    print(('Got certificate: ' + commonname))
    return ok

if len(sys.argv) < 2:
    print('Usage: python server.py PORT')
    sys.exit(1)

dir = os.path.dirname(sys.argv[0])
if dir == '':
    dir = os.curdir

# Initialize context
ctx = SSL.Context(SSL.TLSv1_2_METHOD)
ctx.set_options(SSL.OP_NO_SSLv2)
ctx.set_verify(SSL.VERIFY_NONE, verify_cb) # Demand a certificate
ctx.use_privatekey_file (os.path.join(dir, 'server.pkey'))
ctx.use_certificate_file(os.path.join(dir, 'server.cert'))
ctx.load_verify_locations(os.path.join(dir, 'CA.cert'))

# Set up server
server = SSL.Connection(ctx, socket.socket(socket.AF_INET, socket.SOCK_STREAM))
server.bind(('', int(sys.argv[1])))
server.listen(3) 
server.setblocking(0)

clients = {}
writers = {}
refer = {}
ciphers = {}

with open (os.path.join(os.path.dirname(__file__), 'getUserInfo.html'), "r") as myfile:
    data=myfile.read().replace('\n', '') 

def dropClient(cli, errors=None):
    if errors:
        print('Client %s left unexpectedly:' % (clients[cli],))
        print('  ', errors)
    else:
        print('Client %s left politely' % (clients[cli],))
    del clients[cli]
    if cli in writers:
        del writers[cli]
    if not errors:
        cli.shutdown()
    cli.close()

def insert_data(string, index, data):
    return string[:index] + data + string[index:]

while 1:
    try:
        r, w, _ = select.select([server] + list(clients.keys()), list(writers.keys()), [])
    except:
        break

    for cli in r:
        if cli == server:
            cli,addr = server.accept()
            print('Connection from %s' % (addr,))
            ciphers[cli] = cli.get_cipher_list();
           
            clients[cli] = addr
        else:
            try:
                ret = cli.recv(1024).decode('utf-8')
                refer[cli] = ret
                #print(ret)
            except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                pass
            except SSL.ZeroReturnError:
                dropClient(cli)
            except SSL.Error as errors:
                dropClient(cli, errors)
            else:
                if cli not in writers:
                    writers[cli] = ''
                writers[cli] = writers[cli] + ret

    for cli in w:
        try:
            print(refer[cli])
            for line in refer[cli].split("\n"):
                if "GET" in line and "refer" in line:
                    print(line)
                    host = line.strip().split("refer=")[1].split(" ")[0];
                    print(host)

            jsciphervar = "data[\"ciphers\"] = \"" + (', '.join(ciphers[cli])) + "\";";
            data2 = insert_data(data, len(data)-54, jsciphervar);
            print(data);
            resp = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=utf-8\r\nConnection: close\r\nContent-Length: " + str(len(data2)) + "\r\nAccess-Control-Allow-Origin: *\r\n\r\n" + data2 + "\r\n";
            print(resp)
            ret = cli.send(resp)
            dropClient(cli)
        except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
            pass
        except SSL.ZeroReturnError:
            print("zeroreturn")
            dropClient(cli)
        except SSL.Error as errors:
            dropClient(cli, errors)
            print("sslerror")
        else:
            print("lel")

for cli in clients.keys():
    cli.close()
server.close()
