def ServerHello(sslVersion):
    recordlength = '0035'
    msg_type = '02'
    length = '000031'
    version = '0301'
    random = '51E88C23663EBAB792EE96AC7F19CA3F517B82132EAB3F0D197AF85C51DB5B7C'
    sesison_id = '00'
    cipher = 'c02b'
    compression = '00'
    chunk_ext = '0009FF0100010000230000'
    return bytearray.fromhex('16' + sslVersion + recordlength + msg_type + length + version + random + sesison_id + cipher + compression + chunk_ext)

c = ServerHello('0301')
print(''.join('{:02x}'.format(x) for x in c))