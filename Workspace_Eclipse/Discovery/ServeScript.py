#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler
import os



class ServeScript(BaseHTTPRequestHandler):
    with open (os.path.join(os.path.dirname(__file__), 'checkClient.js'), "r") as myfile:
        txdata=myfile.read().replace('\n', '') 
        
    def do_GET(self):

        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.send_header('Access-Control-Allow-Origin','*')
        self.end_headers()       
        self.wfile.write("<body></body><script>"+self.txdata+"</script>")
     
        return
