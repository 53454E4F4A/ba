from BaseHTTPServer import HTTPServer
from ServeScript import ServeScript;
from os import sys

import SSLCheckServer



try:
    
    SSLCheckServer.runallservers_incremental(sys.argv[1])
    server = HTTPServer(('', int(sys.argv[1])), ServeScript)
    server.serve_forever()

except KeyboardInterrupt:
  
    server.socket.close()