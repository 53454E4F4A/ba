\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {2.1}Pseudocode als SQL-Injection Beispiel}{5}{lstlisting.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {2.2}search.php: Beispiel eines f\"ur XSS anf\"alligen Codes}{6}{lstlisting.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {2.3}search.php: Beispiel Aufruf}{6}{lstlisting.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {2.4}include.php: Beispiel einer direkten Objektreferenz auf eine Datei}{7}{lstlisting.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {2.5}Encapsulated-Header Beispiel}{11}{lstlisting.2.5}
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.1}Client Model}{27}{lstlisting.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.2}Proxy URLPatterns}{30}{lstlisting.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.3}Pseudocode der main Funktion}{30}{lstlisting.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.4}Google Safe Browsing API Request}{30}{lstlisting.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.5}SSLCheck Pseudocode}{31}{lstlisting.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.6}ICAP{\discretionary {-}{}{}}reqmod Pseudocode}{32}{lstlisting.4.6}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.7}Pseudocode DiscoveryServer}{33}{lstlisting.4.7}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.8}Pseudocode runserver}{33}{lstlisting.4.8}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.9}runallservers Beispiel}{34}{lstlisting.4.9}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.10}Pseudocode SSLFootprintServer}{34}{lstlisting.4.10}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.11}Pseudocode DiscoveryScript}{35}{lstlisting.4.11}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.12}InfoServer Pseudocode}{35}{lstlisting.4.12}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.13}REQMOD Methode Pseudocode}{36}{lstlisting.4.13}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4.14}RESPMOD Methode Pseudocode}{37}{lstlisting.4.14}
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.1}Benutzer mit bestimmtem Plugins Blockieren}{40}{lstlisting.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.2}Benutzer \"uber CipherSuite informieren}{41}{lstlisting.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.3}Benutzer \"uber veraltete SSL/TLS Version informieren und blockieren}{42}{lstlisting.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.4}Server CipherSuiten f\"ur SSLv3 von google.com abfragen}{42}{lstlisting.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.5}Zeus Status von benchblog.com}{43}{lstlisting.5.5}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.6}Unzul\"assigen MIME-Type hinzuf\"ugen}{44}{lstlisting.5.6}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.7}include.php}{45}{lstlisting.5.7}
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5.8}Beispiel Befehle und Ausgabe in der Administrator Console}{46}{lstlisting.5.8}
\defcounter {refsection}{0}\relax 
\addvspace {10\p@ }
