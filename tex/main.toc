\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Ziel und Abgrenzung}{1}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Zielgruppe}{2}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Aufbau der Arbeit}{2}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Grundlagen}{4}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}OWASP Top 10 Sicherheitsrisiken von Webanwendungen}{4}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Injection}{5}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Fehler in Authentifizierung und Session Management}{5}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.3}Cross-Site Scripting (XSS)}{6}{subsection.2.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.4}Unsichere direkte Objektreferenzen}{7}{subsection.2.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.5}Sicherheitsrelevante Fehlkonfiguration}{7}{subsection.2.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.6}Verlust der Vertraulichkeit sensibler Daten}{8}{subsection.2.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.7}Fehlerhafte Autorisierung auf Anwendungsebene}{8}{subsection.2.1.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.8}Cross-Site Request Forgery (CSRF)}{8}{subsection.2.1.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.9}Verwendung von Komponenten mit bekannten Schwachstellen}{9}{subsection.2.1.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.10}Ungepr\"ufte Um- und Weiterleitungen}{9}{subsection.2.1.10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Protokolle}{9}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Content Vectoring Protocol}{9}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Internet Content Adaption Protocol}{10}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Web-Application-Firewall}{12}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Modes of Operation}{12}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Security Model}{12}{subsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}Protection Techniques}{13}{subsection.2.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.4}SSL}{13}{subsection.2.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Konzept f\"ur die Softwareentwicklung}{15}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}WAF Servermodule}{15}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}ModSecurity}{15}{subsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}OWASP NAXSI Project}{17}{subsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Analyse}{18}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Zielbestimmung}{18}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Akteure}{18}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Nichtfunktionale Anforderungen}{18}{subsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.4}Funktionale Anforderungen}{19}{subsection.3.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Architektur}{19}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Proxy/WAF}{20}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}DiscoveryServer}{21}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}InfoServer}{23}{subsection.3.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.4}ICAP-Server}{23}{subsection.3.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Implementierung}{25}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Bibiliotheken und Frameworks}{25}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Proxy}{26}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Model}{27}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}View}{27}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}SSLCheck}{31}{subsection.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.4}ICAP-Client}{32}{subsection.4.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}DiscoveryServer}{32}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}ServeScript}{32}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}SSLCheckServer}{33}{subsection.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}SSLFootprintServer}{34}{subsection.4.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.4}DiscoveryScript}{35}{subsection.4.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}InfoServer}{35}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}ICAP-Server}{36}{section.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Tests}{38}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Testumgebung}{38}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Testl\"aufe}{39}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Fallbeispiel 1: Blockieren eines Clients aufgrund Plugins}{40}{subsection.5.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Fallbeispiel 2: Informieren \"uber Cipher Suite}{41}{subsection.5.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.3}Fallbeispiel 3: Blockieren aufgrund von unsicheren SSL/TLS-Versionen}{42}{subsection.5.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.4}Fallbeispiel 4: Informationen \"uber Cipher Suiten des Ziel-Servers}{42}{subsection.5.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.5}Fallbeispiel 5: Blockieren von Ziel-Servern aufgrund von Blacklist-Eintrag}{42}{subsection.5.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.6}Fallbeispiel 6: Blockieren von Content-Types durch ICAP}{43}{subsection.5.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.7}ModSecurity Test}{44}{subsection.5.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.8}Die gef\"ullten Tabellen exemplarisch}{45}{subsection.5.2.8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Fazit}{50}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Zusammenfassung}{50}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Ausblick}{52}{section.6.2}
