from django.conf.urls import patterns, include, url
from django.contrib import admin
from proxy import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'SevenProxies.views.home', name='home'),
      url(r'^admin/', include(admin.site.urls)),
      url(r'.*', views.index, name='index'),
    # url(r'.*', include('proxy.urls')),
    # url(r'^proxy/', include('proxy.urls')),
   
)
