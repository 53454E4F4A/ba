'''
Created on 16.10.2014

@author: jones
'''

from django.conf.urls import patterns, url

from proxy import views

urlpatterns = patterns('',
    url(r'.*', views.index, name='index'),
)