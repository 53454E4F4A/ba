'''
Created on 16.10.2014

@author: jones
'''
import requests
from django.http import HttpResponse


class ProxyMiddleware(object):   

    def process_request(self, request):  
        print('pathr: ' + request.path)
        print(request.path_info[7:16])
        print(request.path_info[7:16])
        if request.path[:7] == '/admin/':
            return None
       # if request.path_info[7:16] == 'localhost':
            #return None
        if request.method == 'GET':      
            print ('ProxyMiddleware processing request')         
            req_url = "http://" + request.get_full_path()[7:]            
            r = requests.get(req_url) 
            print(r.cookies)
           # print(r.text)
            response = HttpResponse(r.content, content_type=r.headers['Content-Type'])
            return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        print ('ProxyMiddleware processing view')
        #  print(view_func)
        
    def process_response(self, request, response):
        print ('ProxyMiddleware processing response')
        #return HttpResponse('testresponse2')
        return response