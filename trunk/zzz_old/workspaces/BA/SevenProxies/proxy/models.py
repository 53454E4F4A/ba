from django.db import models

class Host(models.Model):
    hostname = models.CharField(max_length=255)
    allow_javascript = models.BooleanField(default=False)
    send_cookies = models.BooleanField(default=False)
    set_cookies_allowed = models.BooleanField(default=False)
    allowed = models.BooleanField(default=True)
    
