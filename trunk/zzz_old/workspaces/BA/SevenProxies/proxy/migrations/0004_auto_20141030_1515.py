# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proxy', '0003_auto_20141030_1511'),
    ]

    operations = [
        migrations.RenameField(
            model_name='host',
            old_name='redirect_cookies',
            new_name='send_cookies',
        ),
        migrations.AddField(
            model_name='host',
            name='allow_javascript',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
