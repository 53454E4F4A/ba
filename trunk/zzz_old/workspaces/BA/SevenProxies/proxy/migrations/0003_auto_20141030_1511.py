# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proxy', '0002_auto_20141017_2100'),
    ]

    operations = [
        migrations.CreateModel(
            name='Host',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('hostname', models.CharField(max_length=255)),
                ('redirect_cookies', models.BooleanField(default=False)),
                ('set_cookies_allowed', models.BooleanField(default=False)),
                ('allowed', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='Blacklist',
        ),
        migrations.DeleteModel(
            name='Whitelist',
        ),
    ]
