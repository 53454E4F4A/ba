/*

      QuickTime detector v0.3.2
      By Eric Gerds   http://www.pinlady.net/PluginDetect/


 Note:
  1) In your web page, load the PluginDetect script BEFORE you load this script.
  2) In your web page, have the output <div> BEFORE this script. The <div> looks like this:
       <div id="detectQuickTime_output"></div>
  3) Feel free to modify this script as you wish.


 Speed Issues:
  A) For Internet Explorer, getVersion('QuickTime') takes longer to execute
 than isMinVersion('QuickTime', min). So if speed is an issue for your web page,
 then consider using only isMinVersion('QuickTime', min).
  B) For Internet Explorer, if you do use both getVersion() and isMinVersion() in the
 same web page, then it is faster to use getVersion() FIRST, and THEN isMinVersion().
  C) QuickTime detection generally takes longer on Internet Explorer than on other
 browsers.



*/


(function(){

// Return text message based on plugin detection result
var getStatusMsg = function(obj){
   if (obj.status==1) return "installed & enabled, version is >= " + obj.minVersion;
   if (obj.status===0) return "installed & enabled, version is unknown";
   if (obj.status==-0.1) return "installed & enabled, version is < " + obj.minVersion;
   if (obj.status==-0.2) return "installed but not enabled";
   if (obj.status==-1) return "not installed or not enabled";
   if (obj.status==-3) return "error...bad input argument to PluginDetect method";
   return "unknown";
};   // end of function

var out = document.getElementById("detectQuickTime_output");  // node for output text

// Add text to output node
var docWrite = function(text){
     if (out){
        if (text){
          text = text.replace(/&nbsp;/g,"\u00a0");
          out.appendChild(document.createTextNode(text));
        }
        out.appendChild(document.createElement("br"));
     }
};  // end of function


// Object that holds all data on the plugin
//
// *** Note: @thirdParty can be 0 or 1.
//   If @thirdParty==0, then we only try to detect the genuine Apple QuickTime Player.
//   If @thirdParty==1, then we allow the detection of BOTH genuine and 3rd party QuickTime media players.
//   Detection of 3rd party QuickTime media players only applies to non-Internet Explorer browsers.
//   Only those 3rd party players that adequately mimic the QuickTime player in the navigator[] arrays
//     will be detected (ie. navigator.plugins[i].name == "QuickTime Plugin", etc...)
var P = {name:"QuickTime", status:-1, version:null, minVersion:"7,0,0,0", thirdParty:0};


var $=PluginDetect;



function detect(){

  if ($.getVersion)
  {
    P.version = $.getVersion(P.name, P.thirdParty);
    docWrite("Plugin version: " + P.version);
  }
  if ($.isMinVersion)
  {
    P.status = $.isMinVersion(P.name, P.minVersion, P.thirdParty);
    docWrite("Plugin status: " + getStatusMsg(P));
  }

} // end of detect()


// Detect genuine Apple QuickTime plugin.
P.thirdParty = 0;
docWrite("Detection result for genuine Apple QuickTime Player plugin...");
detect();


// If no detection result for non-IE browsers, then try to detect 3rd party QuickTime players
if (P.status==-1 && !$.browser.isIE){

  P.thirdParty = 1;
  docWrite("");
  docWrite("Detection result for 3rd party QuickTime Player...");
  detect();

}


if ($.browser.isIE)
{
   docWrite("");
   docWrite("ActiveX enabled / ActiveX scripting enabled: " +
     ($.browser.ActiveXEnabled ? "true" : "false [this may prevent the plugin from running in Internet Explorer]")
   );
   docWrite("ActiveX Filtering enabled: " +
     ($.browser.ActiveXFilteringEnabled ? "true [this may prevent the plugin from running in Internet Explorer]" : "false")
   );
}


if ($.isMinVersion)
{
  // QuickTime VR movies are playable by Apple's genuine QuickTime Player 5,0,5 or higher.
  // The genuine QuickTime plugin is present under Windows and Macintosh.
  //
  // Note: iPad QuickTime does not seem to support QTVR.
  // iPad does not reveal QuickTime version either, so $.iMinVersion('QuickTime') == 0.
  // Hence the code "$.isMinVersion() == 1 ? true: false" correctly shows that iPad does not play QTVR.
  //
  // One other requirement for QTVR - during installation, the user can customize the
  // installation to include or exclude QTVR. We assume here that the user allows the
  // installer to include QTVR.
  docWrite("");
  docWrite("Browser can play QuickTime VR (using the genuine QuickTime Player plugin): " +
     ($.isMinVersion(P.name, "5,0,5", 0) == 1 ? "true" : "false"));
}




})();    // end of function


